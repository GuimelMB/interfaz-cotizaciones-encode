#!/usr/bin/env python
from flask import Flask, request, render_template, Markup

app = Flask(__name__, static_url_path='/static') ##Se declara la aplicacion flask

@app.route('/') ## Ubicacion de la pagina principal de raiz... /
def index():
   tvalues = 27 ##valor estatico de Temperatura, agregar variable para volverlo dinamico
   oxvalues = 5 ## lo mismo de arriba pero para el Oxigeno disuelto
   return render_template('index.html',
                          tvalues=tvalues,
                          oxvalues=oxvalues) ## fin de la definicion, renderizar el template y enviar al localhost

@app.route('/graficas')
def graficas():
   labels = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto'] ## vector que contiene las etiquetas en X de las graficas, para mi caso es tiempo
   tvalues = ['27','28','29','30','31','32','33','55'] ##vector de datos que contiene los diferentes valores medidos en el tiempo
   oxvalues = ['1','3','4','6','7','9','11','12'] ##Vector.... valores de Oxigeno disuelto en mi caso
   return render_template('graficas.html', tvalues=tvalues, oxvalues=oxvalues, labels=labels) ## fin de la definicion, renderizar el template y enviar al localhost


if __name__ == '__main__': ## main
   app.run(host= '0.0.0.0', debug = True) ## lanzar el server
